const express = require("express");
const autoLoad = require("./src/common/autoload");
const authContracts = require("#contracts/auth.js");
const app = express();

autoLoad(app, "/api/v2");

//-------------- ALL ROUTES SHOULD BE ONLY WITHIN /services --------------//
// app.get("/", function(req, res) {
//   res.send("Home");
// });

authContracts.authFindUser().then(res => {
  console.log(res);
})

authContracts.getRestData().then(res => {
  console.log(res);
})

app.listen(3000);
console.log("app started");
