const authContracts = require("#contracts/auth.js");

exports.use = require("../middleware/authMiddleware");

exports.get = async function (req, res, next) {
  console.log("Auth root");
  let user = await authContracts.authFindUser();
  res.send(`Auth root is ${user}`);
};