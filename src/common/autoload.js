const path = require('path');
const glob = require('glob');

const setup = (app, endpoint, prefix) => {
	endpoint.app = app;
	app.use((req, res, next) => {
		req.endpoint = endpoint;
		next();
	});
	if (endpoint.use)
		app.use(`${prefix}/${endpoint.url}/`, async function (req, res, next) {
			return endpoint.use(req, res, next);
		});
	if (endpoint.get)
		app.get(`${prefix}/${endpoint.url}/`, async function (req, res, next) {
			return endpoint.get(req, res, next);
		});
	if (endpoint.getOne)
		app.get(`${prefix}/${endpoint.url}/:id`, async function (req, res, next) {
			return endpoint.getOne(req, res, next);
		});
	if (endpoint.post)
		app.post(`${prefix}/${endpoint.url}/`, async function (req, res, next) {
			return endpoint.post(req, res, next);
		});
	if (endpoint.put)
		app.put(`${prefix}/${endpoint.url}/:id`, async function (req, res, next) {
			return endpoint.put(req, res, next);
		});
	if (endpoint.patch)
		app.patch(`${prefix}/${endpoint.url}/:id`, async function (req, res, next) {
			return endpoint.patch(req, res, next);
		});
	if (endpoint.delete)
		app.delete(`${prefix}/${endpoint.url}/:id`, async function (req, res, next) {
			return endpoint.delete(req, res, next);
		});
};

const load = (app, prefix = "") => {
	const directory = path.join(__dirname, "../../src/services");
	glob.sync(`${directory}/**/routes/**/*.js`, {}).forEach(file => {
		let itemName = file.split(".")[0]
			.split('\\').join('/')
			.replace('src/services/', '')
			.replace('/routes/', '/')
			.replace('/index', '');
		console.log(`mounted ${prefix}/${itemName}`);

		let endpoint = require(path.resolve(file));
		if (!endpoint.url) endpoint.url = itemName;
		setup(app, endpoint, prefix);
	});
};

module.exports = load;