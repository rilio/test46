const { findUser } = require("#auth-service");
const { promisify } = require("util");
const { get } = require("request");
const getAsync = promisify(get);


const authFindUser = async () => {
  // May be replaced with rest request if call to microservice
  // We use direct call here
  return await findUser();
};

// Just to show that we can use rest requests here
const getRestData = async () => {
  const url = "https://dog.ceo/api/breeds/image/random";
  let result = undefined;

  try {
    const { statusCode, body } = await getAsync(url);
    if (statusCode === 200) {
      result = body;
    } else {
      // We forward service response
      result = body;
    }
  } catch (e) {
    console.log(e);
  }

  return result;
};

module.exports = {
  authFindUser,
  getRestData
};